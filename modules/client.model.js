const mongodb = require('mongodb')
const mongoose = require('mongoose')


var clientSchema = new mongoose.Schema({
  pNumber: {
    type: Number,
    required: 'Required Field'
  },
  accNumber: {
    type: Number,
//    required: 'Required Field'
  },
  fName: {
    type: String,
    required: 'Required Field'
  },
  lName: {
    type: String,
    required: 'Required Field'
    },
  dob: {
    type: Date,
    required: 'Required Field'
  },
  city: {
    type: String,
    required: 'Required Field'
  },
  created: {
    type: Date,
    required: 'Required Field'
  }
})

mongoose.model("Client", clientSchema)







/*

id, personal_number, account_number, first Name, last name, date of birth, city, created date
*/