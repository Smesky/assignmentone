require('./modules/db')

const express = require('express')
const path = require('path')
const handlebars = require('handlebars')
const exphbs = require('express-handlebars')
const {allowInsecurePrototypeAccess} = require('@handlebars/allow-prototype-access')
const bodyparser = require('body-parser')
// const PORTT = 5000


const clientController = require("./controllers/clientController")

var app = express()


app.use(bodyparser.urlencoded({extended: false}))
app.use(bodyparser.json());


app.get('/', (req, res) => {
    res.send(`
      <h2>Welcome to Evilcorp's own ABC BANK</h2>
      <h3><a href="/client/list">Woe and Behold</a></h3>
    `)
})

app
  .set('views', path.join(__dirname, "/views/"))
  .engine(
    "hbs",
    exphbs({
      handlebars: allowInsecurePrototypeAccess(handlebars),
      extname: "hbs",
      defaultLayout: "mainLayout",
      layoutsDir: __dirname + "/views/layouts",
    })
  )
  .set("view engine", "hbs")
  .listen(process.env.PORT || 5000)
//  .listen( PORT, () => console.log(`Listening on Port ${ PORT }`))
  
app.use("/client", clientController);