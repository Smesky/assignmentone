const express = require('express')
var router = express.Router()
const mongoose = require('mongoose')
const Client = mongoose.model('Client')

router.get('/', (req, res) => {
  res.render("client/addOrEdit", {
    viewTitle: "Insert Client",
  })
})

router.post('/', (req, res) => {
  if (req.body._id == '') {
    insertClient(req, res)
  } else {
    updateClient(req, res)
  }
});

function insertClient(req, res) {
  var client = new Client();
  client.pNumber = req.body.pNumber;
  client.accNumber = req.body.accNumber;
  client.fName = req.body.fName;
  client.lName = req.body.lName;
  client.dob = req.body.dob;
  client.city = req.body.city;
  client.created = req.body.created;
  client.save((err, doc) => {
    if (!err) {
      res.redirect('client/list')
    } else {
      console.log('Encountered error during job: ' + err)
    }
  })
}

function updateClient(req, res) {
  Client.findOneAndUpdate({_id: req.body._id}, req.body, {new: true}, (err, doc) => {
    if (!err) {
      res.redirect("client/list");
    } else {
      console.log('Encountered error during update: ' + err);
    }
  })
}


router.get("/list", (req, res) => {
  Client.find((err, docs) => {
    if (!err) {
      res.render('client/list', {
        list: docs
      })
    } else {
      console.log('Error in retrieval job: ' + err)
    }
  })
})

/*
  Add a button for finding*

  Correlate with:
    const personal_num = await user1.find({personal_number: req.body.id })
      if (personal_num.length) {   // .length will find out if the pnum exists
        console.log("Already in the DB")
      } else {
        res.render("client/addOrEdit")
      }
*/


router.get('/:id', (req, res) => {
  Client.findById(req.params.id, (err, doc) => {
    if (!err) {
      res.render("client/addOrEdit", {
        viewTitle: "Update Client",
        client: doc,
      })
      console.log(doc);
    } 
  })
})


router.get('/delete/:id', (req, res)  => {
  Client.findByIdAndRemove(req.params.id, (err, doc) => {
    if (!err) {
      res.render("client/list")
    } else {
      console.log("Error in deletion job: " + err)
    }
  })
})

module.exports = router